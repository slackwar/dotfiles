if !has('nvim')
    set t_Co=256      " force 256 colors
    set nocompatible  " be iMproved, required
endif
filetype off          " required for Vundle

"Vundle configuration
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
"===============================================================================
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

"here all Plugin stuff should go

"Intellectual autocompletion for Vim
Plugin 'Valloric/YouCompleteMe'

" syntax checking
if has('nvim')
    Plugin 'benekastah/neomake'
    let s:neomake = 1
else
    Plugin 'scrooloose/syntastic'
    let s:syntastic = 1
endif

"Handy commenting plugin
Plugin 'scrooloose/nerdcommenter'

"File manager
Plugin 'scrooloose/nerdtree'

"Intellectual autocompletion for Python
"YCM uses Jedi for python completion (no need for a separate plugin)
"Plugin 'davidhalter/jedi-vim'

"Full path fuzzy file, buffer, mru, tag, ... finder for Vim
Plugin 'ctrlpvim/ctrlp.vim'

"Support for ag (the_silver_searcher)
Plugin 'rking/ag.vim'

"Support for ack as well
Plugin 'mileszs/ack.vim'

"Autocompletion for quotes, parens, brackets, etc.
Plugin 'Raimondi/delimitMate'

"Enhanced python syntax highlighting.
Plugin 'hdima/python-syntax'

"Operations with brackets and other surroundings
Plugin 'tpope/vim-surround'

"Git integration for Vim
Plugin 'tpope/vim-fugitive'

"Mercurial integration
Plugin 'ludovicchabant/vim-lawrencium'

"Vim Tmux Navigator
Plugin 'christoomey/vim-tmux-navigator'

"Autoread for files changed in backgroound"
Plugin 'djoshea/vim-autoread'

"PEP8 indentation
Plugin 'hynek/vim-python-pep8-indent'

"Indent lines
Plugin 'Yggdroot/indentLine'

"Colorscheme as plugin
Plugin 'Zenburn'
Plugin 'nanotech/jellybeans.vim'
Plugin 'NLKNguyen/papercolor-theme'
Plugin 'alessandroyorba/despacio'


call vundle#end()
"===============================================================================
filetype plugin indent on

syntax enable
"set mouse=a
set number
set fileformat=unix
set splitbelow

"set autoread
"autocmd FileChangedShell * echo "Warning: File changed on disk"

"80 char column highlight
set colorcolumn=80

set shiftwidth=4
set tabstop=4       "how many spaces you will see
set softtabstop=0
set autoindent
"set smartindent
set nocindent
"set cinoptions=(0,u0,U0
set expandtab       "exchange tab on several spaces
"set noexpandtab
"set smarttab
set listchars=tab:>-,eol:$,trail:.,extends:#

set laststatus=2
set statusline=%<%f\ %h%m%r\ SYN:%{&syntax}%=(%b\ 0x%B)\ %#WildMenu#%l,%c%*\ %p%%\ of\ %L\ lines
let &statusline="%<%f %h%m%r%y[%{&fenc.','.&fileformat}]%=(%b 0x%B) %#WildMenu#%l,%c%* %p%% of %LL %{getfsize(expand('%:p'))}"

"Encoding menu
set wildmenu
set wcm=<Tab>
menu Encoding.koi8-r       :e ++enc=koi8-r<CR>
menu Encoding.windows-1251 :e ++enc=cp1251<CR>
menu Encoding.ibm-866      :e ++enc=ibm866<CR>
menu Encoding.utf-8        :e ++enc=utf-8 <CR>
map <F6> :emenu Encoding.<TAB>

map <F2> :NERDTreeToggle<CR>
map <F3> :set invlist<CR>

"============Highlighting options================
" highlight trailing spaces
au BufNewFile,BufRead * let b:mtrailingws=matchadd('ErrorMsg', '\s\+$', -1)

" highlight tabs between spaces
"au BufNewFile,BufRead * let b:mtabbeforesp=matchadd('ErrorMsg', '\v(\t+)\ze( +)', -1)
"au BufNewFile,BufRead * let b:mtabaftersp=matchadd('ErrorMsg', '\v( +)\zs(\t+)', -1)
"
"show pair bracket for html/xml
set matchpairs+=<:>

" disable matches in help buffers
au BufEnter,FileType help call clearmatches()

"do not jump on *, highlight only
set nohlsearch
nnoremap * *N
nnoremap <F8> :set invhlsearch<CR>
"============Highlighting options end============

"============Auto commands=======================

" Neomake {{{
" Make neomake far more quiet and only show errors
let g:neomake_verbose = 0
augroup personal_neomake
    au!
    au BufWritePost,BufEnter * if exists('s:neomake') | Neomake | endif
    "This doesn't work, although I would like it to (should give lint as I type)
    "au TextChanged * if exists('s:neomake') | update | Neomake endif
augroup END
" }}}

"================================================


"Netrw settings
let g:netrw_winsize=50
let g:netrw_altv=1
let g:netrw_fastbrowse=2
let g:netrw_keepdir=0
"let g:netrw_liststyle=2
let g:netrw_retmap=1
let g:netrw_silent=1
let g:netrw_special_syntax=1

"ag.vim settings
"search from project root (.hg file place)
let g:ag_working_path_mode="r"

"ack plugin settings
if executable('ag')
	let g:ackprg = 'ag --vimgrep'
endif

"Python syntax highlighting settings (hdima/python-syntax)
let python_highlight_all=1

"Close GetDoc window (pclose) after Autocomplete
autocmd CompleteDone * pclose

"indentLine settings
let g:indentLine_color_term = 239

"preventing IndentLine plugin from hiding symbols (i.e. json quotes)
let g:indentLine_conceallevel = 0

"ctrlp.vim settings
"search from project root then current dir
"let g:ctrlp_working_path_mode="ra"
"let g:ctrlp_map = '<c-p>'
"let g:ctrlp_cmd = 'CtrlP'
"set wildignore+=*.so,*.swp,*.zip
"let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'

"map ^[[5~ <PgUp>
"map ^[[6~ <PgDn>
"imap ^[[5~ <PgUp>
"imap ^[[6~ <PgDn>
"map <ESC>[5^ <C-PageUp>
"map <ESC>[6^ <C-PageDown>
"map ^[[5;5~ <C-PageUp>
"map ^[[6;5~ <C-PageDown>
"nnoremap <C-PageDown> :bn!<CR>
"nnoremap <C-PageUp> :bp!<CR>

"Zenburn colorscheme options
"set cursorline cursorcolumn
"let g:zenburn_high_Contrast=1
"let g:zenburn_alternate_Visual=1
"let g:zenburn_alternate_Error=1
"let g:zenburn_alternate_Include=1
"let g:zenburn_force_dark_Background=1
"let g:zenburn_unified_CursorColumn=1
"let g:zenburn_old_Visual=1
"colorscheme zenburn

"Jellybeans colorscheme options
"colorscheme jellybeans

"Despacio colorscheme options

"medium gray background
"let g:despacio_Sunset = 1

"dark gray background
let g:despacio_Twilight = 1

"almost black background
"let g:despacio_Midnight = 1

"black background
"let g:despacio_Pitch = 1
colorscheme despacio


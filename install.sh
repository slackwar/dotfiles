#!/usr/bin/bash

cp bash_aliases ../.bash_aliases
cp bash_profile ../.bash_profile  
cp bashrc ../.bashrc
cp tmux.conf ../.tmux.conf
cp vimrc ../.vimrc
cp Xdefaults ../.Xdefaults

export ZSH=/home/afomichev/.oh-my-zsh

#ZSH_THEME="robbyrussell"
ZSH_THEME="gentoo"

plugins=(git mercurial colored-man-pages colorize common-aliases python pip perl)

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)

source $ZSH/oh-my-zsh.sh

# User configuration

#PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"
PATH=$HOME/code/internal/nb-devtools/bin:$PATH:$HOME/.local/bin:$HOME/bin
PYTHONPATH=$PYTHONPATH:~/code/internal/nb-devtools/modules
export PATH
export PYTHONPATH
export EDITOR=nvim
export BROWSER="firefox"

HISTSIZE=10000
SAVEHIST=10000
HISTFILESIZE=10000
HISTFILE=$HOME/.histfile

hg-status() {
	echo -n "$(~/code/internal/nb-devtools/fast-hg-prompt/fast-hg-status)"
}

hg-branch() {
	here="$(realpath "$PWD")"
	while [ -n "$here" -a / != "$here" ]; do
		if [ -e "$here/.hg" ]; then
			echo -n $(cat "$here/.hg/branch")
			break
		fi
		here="$(dirname "$here")"
	done
}

set-hg-branch() {
	BRANCH_COLOR="%{$fg_bold[green]%}"
	if [ "$(hg-status)" ]; then
		BRANCH_COLOR="%{$fg_bold[red]%}"
	fi
	branch_name=$(hg-branch)
	if [ "$branch_name" ]; then
        print "${BRANCH_COLOR}[${branch_name}]%{$reset_color%}"
	fi
}

set_prompt() {
    PROMPT='%{$fg[orange]%}%n@%m%{$reset_color%}:%{$fg_bold[blue]%}%d%{$reset_color%} %{$(set-hg-branch)%}%# '
}

setopt prompt_subst
set_prompt


alias zshconfig="mate ~/.zshrc"
alias ohmyzsh="mate ~/.oh-my-zsh"

alias ls="ls --color"
alias ll='ls -alF --group-directories-first'

alias tmux="TERM=rxvt-256color tmux"
alias vim="nvim"
alias cl="clear"

# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle :compinstall filename '/home/afomichev/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=10000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize


# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

determine_color_support() {
	# set a fancy prompt (non-color, unless we know we "want" color)
	case "$TERM" in
		xterm-color) color_prompt=yes;;
		rxvt-256color) color_prompt=yes;;
	esac
}

# Color constants
noColor='\[\e[0m\]'
blackColor='\[\e[0;30m\]'
redColor='\[\e[0;31m\]'
greenColor='\[\e[0;32m\]'
yellowColor='\[\e[0;33m\]'
blueColor='\[\e[0;34m\]'
purpleColor='\[\e[0;35m\]'
cyanColor='\[\e[0;36m\]'
greyColor='\[\e[0;37m\]'

boldGrayColor='\[\e[1;30m\]'
boldRedColor='\[\e[1;31m\]'
boldGreenColor='\[\e[1;32m\]'
boldYellowColor='\[\e[1;33m\]'
boldBlueColor='\[\e[1;34m\]'
boldPurpleColor='\[\e[1;35m\]'
boldCyanColor='\[\e[1;36m\]'
boldWhiteColor='\[\e[1;37m\]'

hg-status() {
	echo -n "$(~/code/bb/internal/nb-devtools/fast-hg-prompt/fast-hg-status)"
}

hg-branch() {
	here="$(realpath "$PWD")"
	while [ -n "$here" -a / != "$here" ]; do
		if [ -e "$here/.hg" ]; then
			echo -n $(cat "$here/.hg/branch")
			break
		fi
		here="$(dirname "$here")"
	done
}

set-hg-branch() {
	BRANCH_COLOR=${greenColor}
	dirty_flag=
	if [ "$(hg-status)" ]; then
		BRANCH_COLOR=${boldRedColor}
		dirty_flag="*"
	fi
	branch_name=$(hg-branch)
	if [ "$branch_name" ]; then
		echo -n " ${BRANCH_COLOR}[${dirty_flag}${branch_name}]${noColor}"
	fi
}

set_bash_prompt() {
	determine_color_support
	branch_section=$(set-hg-branch)

	if [ "$color_prompt" = yes ]; then
		PS1="${yellowColor}\u${noColor}:${boldBlueColor}\w${noColor}${branch_section}\n\$ "
	else
		PS1='\u:\w${branch_section}\n\$ '
	fi
}

force_color_prompt=yes
export PROMPT_COMMAND=set_bash_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*)
    color_prompt=yes
    ;;
screen)
    color_prompt=yes
    ;;
*256*)
    color_prompt=yes
    ;;
*)
    ;;
esac

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# add my environment variables
if [ -f ~/.bash_exports ]; then
	. ~/.bash_exports
fi

# add vendor switching functions
if [ -f ~/.bash_vendor_functions ]; then
    . ~/.bash_vendor_functions
fi
